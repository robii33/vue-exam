import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

import main from "@/pages/main"
import task from "@/pages/task"

import '@/assets/styles/main.scss';

const routes = [
  {
    path: '/',
    component: main,
  },
  {
    path: '/task/:id',
    component: task,
  }
]

const router = new VueRouter(
  {
    routes,
  }
)


new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
